package taller.mundo;

import java.util.ArrayList;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;

	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		int col = (int) Math.round(Math.random()*(tablero[0].length-1));
		registrarJugada(col);
	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		boolean rta = false;
		for (int row = tablero.length-1; row >= 0 && !rta; row--) 
		{
			if(tablero[row][col].equals("___"))
			{
				tablero[row][col] = jugadores.get(turno).darSimbolo();
				rta = true;
				terminar(row, col, 0, 0);
			}
		}
		return rta;
	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col, int c, int pos)
	{
		long TInicio, TFin, tiempo; 
		TInicio = System.currentTimeMillis();
		int max = 4;
		c += 1;
		if(c >= max)
		{
			finJuego = true;
			return true;
		}
		System.out.println("Contador: " + c);
		boolean rta = true;
		if(col-1 >= 0 && (pos == 0 || pos == 1) && !tablero[fil][col-1].equals("___"))
		{
			if(rta && fil-1 >= 0 && tablero[fil][col].equals(jugadores.get(turno).darSimbolo()))
			{
				rta = terminar(fil-1, col-1, c, 1);
			}
			else
			{
				rta = true;
				c = 1;
			}
		}
		if((pos == 0 || pos == 2) && col+1 < tablero[0].length && !tablero[fil][col+1].equals("___"))
		{
			if(rta && fil-1 >= 0 && tablero[fil-1][col+1].equals(jugadores.get(turno).darSimbolo()))
			{
				rta = terminar(fil-1, col+1, c, 2);
			}
			else
			{
				rta = true;
				c = 1;
			}
		}
		if(pos == 0 || pos == 3)
		{
			if(rta && fil-1 >= 0 && tablero[fil-1][col].equals(jugadores.get(turno).darSimbolo()))
			{
				rta = terminar(fil-1, col, c, 3);
			}
			else
			{
				rta = true;
				c = 1;
			}
		}
		if(pos == 0 || pos == 4)
		{
			if(rta && (pos == 0 || pos == 4) && col-1 >= 0 && tablero[fil][col-1].equals(jugadores.get(turno).darSimbolo()))
			{
				rta = terminar(fil, col-1, c, 4);
			}
			else
			{
				rta = true;
				c = 1;
			}
		}
		if(pos == 0 || pos == 5)
		{
			if(rta && col+1 < tablero[0].length && tablero[fil][col+1].equals(jugadores.get(turno).darSimbolo()))
			{
				rta = terminar(fil, col+1, c, 5);
			}
			else
			{
				rta = true;
				c = 1;
			}
		}
		if(pos == 0 || pos == 6)
		{
			if(rta && fil+1 < tablero.length && col-1 >= 0 && tablero[fil+1][col-1].equals(jugadores.get(turno).darSimbolo()))
			{
				rta = terminar(fil+1, col-1, c, 6);
			}
			else
			{
				rta = true;
				c = 1;
			}
		}
		if(pos == 0 || pos == 7)
		{
			if(rta && (pos == 0 || pos == 7) && fil+1 < tablero.length && tablero[fil+1][col].equals(jugadores.get(turno).darSimbolo()))
			{
				rta = terminar(fil+1, col, c, 7);
			}
			else
			{
				rta = true;
				c = 1;
			}
		}
		if(pos == 0 || pos == 8) 
		{
			if(rta && (pos == 0 || pos == 8) && fil+1 < tablero.length && col+1 < tablero[0].length && tablero[fil+1][col+1].equals(jugadores.get(turno).darSimbolo()))
			{
				rta = terminar(fil+1, col+1, c, 8);
			}
			else
			{
				rta = true;
				c = 1;
			}
		}
		if(!finJuego && pos == 0)
		{
			cambiarTurno();
		}
		TFin = System.currentTimeMillis();
		tiempo = TFin - TInicio;
		System.out.println("Tiempo de ejecución en milisegundos: " + tiempo);
		return finJuego;
	}

	/**
	 * Cambia el turno entre los jugadores
	 */
	public void cambiarTurno()
	{
		if(turno == 0)
		{
			turno = 1;
		}
		else
		{
			turno = 0;
		}
		atacante = jugadores.get(turno).darNombre();
	}

	/**
	 * Reinicia el juego
	 */
	public void reiniciar()
	{
		finJuego = false;
	}

}
