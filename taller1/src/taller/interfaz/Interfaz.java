package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		sc.useDelimiter("\n");
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				System.out.println("--------------------------------------------------------");
				continue;
			}
			break;
		}
	}

	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
		while(true)
		{
			System.out.println("Ingrese el nombre del primer jugador: ");
			String nombre1 = sc.next();

			System.out.println("Ingrese el simbolo que desea usar el primer juegador: ");
			String simbolo1 = sc.next().toUpperCase();

			System.out.println("Ingrese el nombre del segundo jugador: ");
			String nombre2 = sc.next();

			System.out.println("Ingrese el simbolo que desea usar el segundo jugador: ");
			String simbolo2 = sc.next().toUpperCase();

			if(!simbolo1.equals(simbolo2) && !nombre1.equals(nombre2))
			{
				jugadores.add(new Jugador(nombre1, simbolo1));
				jugadores.add(new Jugador(nombre2, simbolo2));
				break;
			}
			System.out.println("Lo jugadores no pueden tener el mismo nombre o simbolo");
			System.out.println("--------------------------------------------------------");
		}
		while(true)
		{
			try
			{
				System.out.println("Numero de filas del tablero: ");
				int fil = Integer.parseInt(sc.next());
				System.out.println("Numero de columnas del tablero: ");
				int col = Integer.parseInt(sc.next());
				System.out.print("Numero de fichas seguidas necesarias para ganar: ");
				int cantGanadora = Integer.parseInt(sc.next());
				if(fil < 4 || col < 4) 
				{
					System.out.println("El tablero debe ser de mínimo 4x4");
					continue;
				}
				else
				{
					juego = new LineaCuatro(jugadores, fil, col, cantGanadora);
					break;
				}
			} 
			catch (Exception e) 
			{
				System.out.println("Comando inválido");
				System.out.println("--------------------------------------------------------");
				continue;
			}
		}
		juego();
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		boolean empate = false;
		imprimirTablero();
		while(!juego.fin())
		{
			System.out.println("Es el turno de " + juego.darAtacante());
			System.out.println("Ingrese la columna en la que desea colocar su ficha (del 1 al "+juego.darTablero()[0].length+")");
			try {
				int col = Integer.parseInt(sc.next())-1;
				if(col > juego.darTablero()[0].length || col < 0)
				{
					System.out.println("Excede el limite de las columnas");
					continue;
				}
				juego.registrarJugada(col);
			} catch (Exception e) {
				System.out.println("Comando inválido");
				System.out.println("--------------------------------------------------------");
				continue;
			}
			empate = imprimirTablero();
			if(empate)
				break;
		}
		if(empate)
			System.out.println("Es un empate");
		else
			System.out.format("El ganador de la partida fue %s\n", juego.darAtacante());
		while (true)
		{
			System.out.println("-Menú Fin de juego-");
			System.out.println("Ingrese un comando:");
			System.out.println("1. Reiniciar el juego");
			System.out.println("2. Salir");
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					juego.reiniciar();
					juego();
				}
				else if(opt==2)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				System.out.println("--------------------------------------------------------");
				continue;
			}
		}	
	}

	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();

		System.out.println("Ingrese el nombre del primer jugador:");
		String nombre1 = sc.next();

		System.out.println("Ingrese el simbolo que desea usar el primer juegador:");
		String simbolo1 = sc.next().toUpperCase();

		jugadores.add(new Jugador(nombre1, simbolo1));
		if(simbolo1.equals("X") || simbolo1.equals("_X_") || simbolo1.equals("-X-"))
		{
			jugadores.add(new Jugador("Maquina", "_O_"));
		}
		else 
		{
			jugadores.add(new Jugador("Maquina", "_X_"));
		}
		while(true)
		{
			try {
				System.out.print("Numero de filas del tablero: ");
				int fil = Integer.parseInt(sc.next());

				System.out.print("Numero de columnas del tablero: ");
				int col = Integer.parseInt(sc.next());

				System.out.print("Numero de fichas seguidas necesarias para ganar: ");
				int cantGanadora = Integer.parseInt(sc.next());

				if(fil < 4 || col < 4) 
				{
					System.out.println("El tablero debe ser de mínimo 4x4");
					continue;
				}
				else
				{

					juego = new LineaCuatro(jugadores, fil, col, cantGanadora);
					break;
				}
			} catch (Exception e) {
				System.out.println("Comando inválido");
				System.out.println("--------------------------------------------------------");
				continue;
			}
		}
		juegoMaquina();
	}

	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		boolean empate = false;
		imprimirTablero();
		while(!juego.fin())
		{
			System.out.println("Es el turno de " + juego.darAtacante());
			if(juego.darAtacante() != "Maquina")
			{
				System.out.println("Ingrese la columna en la que desea colocar su ficha (del 1 al "+juego.darTablero()[0].length+")");
				try {
					int col = Integer.parseInt(sc.next())-1;
					if(col > juego.darTablero()[0].length || col < 0)
					{
						System.out.println("Excede el limite de las columnas");
						continue;
					}
					juego.registrarJugada(col);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Comando inválido");
					System.out.println("--------------------------------------------------------");
					continue;
				}
			}
			else 
			{
				juego.registrarJugadaAleatoria();
			}
			empate = imprimirTablero();
			if(empate)
				break;
		}
		if(empate)
			System.out.println("Es un empate");
		else
			System.out.format("El ganador de la partida fue %s\n", juego.darAtacante());
		
		while (true)
		{
			System.out.println("---Menú Fin de juego---");
			System.out.println("Ingrese un comando:");
			System.out.println("1. Reiniciar el juego");
			System.out.println("2. Salir");
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					juego.reiniciar();
					juegoMaquina();
				}
				else if(opt==2)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					System.out.println("--------------------------------------------------------");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				System.out.println("--------------------------------------------------------");
				continue;
			}
		}
	}

	/**
	 * Imprime el estado actual del juego e indica si aún hay casillas jugables
	 * True si hay casillas jugables, False de lo contrario
	 */
	public boolean imprimirTablero()
	{
		boolean rta = true;
		for (int i = 0; i < juego.darTablero().length; i++) {
			for (int j = 0; j < juego.darTablero()[0].length; j++) {
				System.out.print(juego.darTablero()[i][j]+" ");
				if(juego.darTablero()[i][j].equals("___"))
					rta = false;
			}
			System.out.println("");
		}
		return rta;
	}
}
