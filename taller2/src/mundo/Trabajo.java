package mundo;

import java.util.Date;

public class Trabajo extends Evento {

	public enum  TipoEventoTrabajo {
		JUNTA,
		ALMUERZO,
		CENA,
		COCTEL,
		PRESENTACION,
		QUINCENA,
		ENTREGA
	}
	
	protected TipoEventoTrabajo tipo;
	
	public Trabajo(Date pFecha, String pLugar, boolean pObligatorio, boolean pFormal, TipoEventoTrabajo tipo)
	{
		super(pFecha, pLugar, pObligatorio, pFormal);
		this.tipo = tipo;
	}
}
